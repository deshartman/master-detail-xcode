//
//  DetailViewController.h
//  MD2
//
//  Created by Des Hartman on 5/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface DetailViewController : UITableViewController

@property (strong, nonatomic) Event * eventDetail;

@property (strong, nonatomic) IBOutlet UILabel *detailLabel;

@property (strong, nonatomic) IBOutlet UITextField *timeStampText;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
