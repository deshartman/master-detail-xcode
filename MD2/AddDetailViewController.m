//
//  AddDetailViewController.m
//  MD2
//
//  Created by Des Hartman on 6/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AddDetailViewController.h"

@interface AddDetailViewController()
	@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@end

@implementation AddDetailViewController

@synthesize delegate = _delegate;
@synthesize datePicker = _datePicker;
@synthesize currentDate = _currentDate;

-(void)setCurrentDate:(NSDate *)initialDate {
	if(_currentDate != initialDate) {
		_currentDate = initialDate;
	}
	self.datePicker.date = _currentDate;
}



-(IBAction)cancel:(UIBarButtonItem *)sender {
	[self.delegate didSelectCancel:self];
}

-(IBAction)add:(UIBarButtonItem *)sender {
	self.currentDate = self.datePicker.date;
	
	[self.delegate didSelectDone:self];
}





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}




- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
