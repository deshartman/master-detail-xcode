//
//  MasterViewController.h
//  MD2
//
//  Created by Des Hartman on 5/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterModel.h"
#import "AddDetailViewController.h"

///#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <CoreDataModelDelegate, AddTimeStampDelegate>

-(IBAction)add:(UIBarButtonItem *)sender;
//-(IBAction)edit:(UIBarButtonItem *)sender;

@end
