//
//  MasterModel.m
//  MD2
//
//  Created by Des Hartman on 5/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MasterModel.h"

@interface MasterModel()
-(void)fetchData;
@end

@implementation MasterModel {
	int sectionCount;
	int rowsPerSection;
}

@synthesize managedObjectContext = __managedObjectContext;
@synthesize delegate = _delegate;
@synthesize objectArray = _objectArray;	// of IndexPath Objects




- (id)init {
    if (self = [super init]) {
		
		sectionCount = 1;
		rowsPerSection = [self.objectArray count];
    }
    return self;
}



// Customize the number of sections in the table view.
- (NSInteger)numberOfSections {
	return sectionCount;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
	return rowsPerSection;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
	// Get an Item from Core Data at a particular position
	return [self.objectArray objectAtIndex:indexPath.row];
}

////////// ADD, DELETE, EDIT //////////

-(void)add:(NSDate *)date {
	// Create a new instance of the entity managed by the fetched results controller.
	Event *event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    event.timeStamp = date; //[NSDate date];
	
	//	NSString *now = [event.timeStamp description];
	//NSString *now2 = [[NSDate date] description];
    
    // Save the context.
    NSError *error = nil;
	
	BOOL savedSuccessfully = [self.managedObjectContext save:&error];
	
    if (!savedSuccessfully) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
	[self fetchData]; //Might be a bit slow. Might be better to add to Array and model
	
}

-(void)delete:(NSIndexPath *)indexPath {
	
	[self.managedObjectContext deleteObject:[self.objectArray objectAtIndex:indexPath.row]];
	
	// Save the context.
	NSError *error = nil;
	if (![self.managedObjectContext save:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	[self fetchData]; //Might be a bit slow. Might be better to add to Array and model
}

#pragma mark - Managed Object Context

- (NSManagedObjectContext *)managedObjectContext
{
	if (__managedObjectContext == nil)
    {
		// Get the Global Persistent Store Coordinator
		NSPersistentStoreCoordinator *coordinator = [(AppDelegate *) [[UIApplication sharedApplication] delegate] persistentStoreCoordinator];	
		if (coordinator != nil)
		{
			// Create a new managed Object Context for this model (Thread Safe)
			__managedObjectContext = [[NSManagedObjectContext alloc] init];
			[__managedObjectContext setPersistentStoreCoordinator:coordinator];
		}
	}
    return __managedObjectContext;	
}

#pragma mark - Fetched results controller

- (NSArray *)objectArray {
    if (_objectArray == nil) {
		[self fetchData];
	}
    return _objectArray;
}

-(void)fetchData {
	// Create the fetch request for the entity.
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	// Edit the entity name as appropriate.
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
	[fetchRequest setEntity:entity];
	
	// Set the batch size to a suitable number.
	//[fetchRequest setFetchBatchSize:20];
	
	// Edit the sort key as appropriate.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
	NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	
	NSError *error = nil;
	self.objectArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	if(!self.objectArray) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
   
	
	// Update the local cache values for sections and rows per section arrays.
	sectionCount = 1;
	rowsPerSection = [self.objectArray count]; // ONE section ONLY for now
	
	// NOTIFY the interested parties
	[self.delegate modelDidChangeContent:self];
}



@end
