//
//  MasterModel.h
//  MD2
//
//  Created by Des Hartman on 5/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Event.h"

@class MasterModel;	// forward declaration

@protocol CoreDataModelDelegate

	@optional
	- (void)modelDidChangeContent:(MasterModel *)model;

@end




@interface MasterModel : NSObject

/* Delegate that is notified when the result set changes.
 */
@property(nonatomic, assign) id <CoreDataModelDelegate> delegate;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) NSArray *objectArray;

-(NSInteger)numberOfSections;
-(NSInteger)numberOfRowsInSection:(NSInteger)section;
-(id)objectAtIndexPath:(NSIndexPath *)indexPath;
-(void)add:(NSDate *)date;
-(void)delete:(NSIndexPath *)indexPath;


@end

