//
//  AddDetailViewController.h
//  MD2
//
//  Created by Des Hartman on 6/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AddDetailViewController;

@protocol AddTimeStampDelegate

@optional
- (void)didSelectCancel:(AddDetailViewController *)controller;
- (void)didSelectDone:(AddDetailViewController *)controller;

@end


@interface AddDetailViewController : UIViewController

//@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *currentDate;					// this is the model object passed in

-(IBAction)cancel:(UIBarButtonItem *)sender;
-(IBAction)add:(UIBarButtonItem *)sender;

@property(nonatomic, assign) id <AddTimeStampDelegate> delegate;


@end
