//
//  MasterViewController.m
//  MD2
//
//  Created by Des Hartman on 5/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Event.h"
#import "AddDetailViewController.h"

@interface MasterViewController ()
// MODEL
@property (nonatomic, retain) MasterModel *masterModel;

@property (nonatomic, retain) AddDetailViewController *addDetailedViewController;
// VIEW
// Storyboard hooks


///- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MasterViewController

@synthesize masterModel = _masterModel;
@synthesize addDetailedViewController = _addDetailedViewController;

// Lazy initialise it
-(MasterModel *)masterModel
{
	if (!_masterModel) {
		_masterModel = [[MasterModel alloc] init];
	}
	return _masterModel;
}

-(IBAction)add:(UIBarButtonItem *)sender {
	[self.masterModel add:[NSDate date]];
}

//-(IBAction)edit:(UIBarButtonItem *)sender {}

- (void)awakeFromNib
{
	///    [super awakeFromNib];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	// Set up the edit and add buttons.
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	
	self.masterModel.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.masterModel numberOfSections];
	///	return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.masterModel numberOfRowsInSection:section];	
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
	// Dequeue or create a cell of the appropriate type.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
	
    // Get the object to display and set the value in the cell.
	Event *eventAtIndex = [self.masterModel objectAtIndexPath:indexPath];
	cell.textLabel.text = [eventAtIndex.timeStamp description];
	
	return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
		[self.masterModel delete:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		//		NSManagedObject *selectedObject = [self.masterModel objectAtIndexPath:indexPath];
		Event *eventObject = [self.masterModel objectAtIndexPath:indexPath];
        [[segue destinationViewController] setEventDetail:eventObject];
    }
	
	if([[segue identifier] isEqualToString:@"ShowAddView"]) {
		
		AddDetailViewController *addDetailViewController = [[[segue destinationViewController] childViewControllers] objectAtIndex:0];

		addDetailViewController.delegate = self;
		NSDate *initialDate = [NSDate date];
		[addDetailViewController setCurrentDate:initialDate];		
	}
}

/////////////////////   


- (void)modelDidChangeContent:(MasterModel *)model
{
	[self.tableView reloadData];
}

-(void)didSelectDone:(AddDetailViewController *)controller {
	[self.masterModel add:controller.currentDate];
	[self dismissModalViewControllerAnimated:YES];
}

-(void) didSelectCancel:(AddDetailViewController *)controller {
	[self dismissModalViewControllerAnimated:YES];
	
	//self.navigationController [[controller parentViewController] removeFromParentViewController];
}

@end
